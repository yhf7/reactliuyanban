import React, { Component } from 'react'
import Comment from './Comment'
import PropTypes from 'prop-types'

class CommentList extends Component {

  static propTypes = {
    comments: PropTypes.array,
    onDeleteComment: PropTypes.func
  }

  static defaultProps = {
    comments: []
  }

  /**
   * 删除函数
   * @param {*} index 
   */
  handleDeleteComment(index) {
    if (this.props.onDeleteComment) {
      this.props.onDeleteComment(index)
    }
  }

  render() {
    return (
      <div>{ this.props.comments.map((comment, i) => <Comment comment={ comment } onDeleteComment={ this.handleDeleteComment.bind(this) } index={i} key={i}/> )}</div>
    )
  }
}

export default CommentList