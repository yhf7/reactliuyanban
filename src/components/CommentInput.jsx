import React, { Component } from 'react'
import PropTypes from 'prop-types'

export default class CommentInput extends Component {
  static propTypes = {
    username: PropTypes.any,
    onSubmit: PropTypes.func,
    onUserNameInputBlur: PropTypes.func
  }

  static defaultProps = {
    username: ''
  }

  constructor(props) {
    super(props)
    this.state = {
      username: props.username, // 从 props 上取 username 字段
      content: ''
    }
  }

  /**
   * 组件运行完时焦点聚焦
   */
  componentDidMount() {
    this.textarea.focus()
  }

  /**
 * 用户名input失去焦点触发存储用户名
 * @param {*} event 用户名
 */
  handleUsernameBlur(event) {
    if (this.props.onUserNameInputBlur) {
      this.props.onUserNameInputBlur(event.target.value)
    }
  }

  /**
   * 接收赋值用户名
   * @param {*} event value
   */
  handleUsernameChange(event) {
    this.setState({
      username: event.target.value
    })
  }

  /**
   * 接收赋值评论内容
   * @param {*} event value
   */
  handleContentChange(event) {
    this.setState({
      content: event.target.value
    })
  }

  /**
   * 点击事件，发布
   */
  handleSubmit() {
    if (this.props.onSubmit) {
      // 调用父级onSubmit传入参数
      this.props.onSubmit({
        username: this.state.username,
        content: this.state.content,
        createdTime: +new Date()
      })
    }
    this.setState({ content: '' })
  }

  render() {
    return (
      <div className='comment-input'>
        <div className='comment-field'>
          <span className='comment-field-name'>用户名：</span>
          <div className='comment-field-input'>
            <input onBlur={ this.handleUsernameBlur.bind(this) } value={ this.state.username } onChange={ this.handleUsernameChange.bind(this) } />
          </div>
        </div>
        <div className='comment-field'>
          <span className='comment-field-name'>评论内容：</span>
          <div className='comment-field-input'>
            <textarea ref={ (textarea) => this.textarea = textarea } value={ this.state.content } onChange={ this.handleContentChange.bind(this) } />
          </div>
        </div>
        <div className='comment-field-button' onClick={ this.handleSubmit.bind(this) }>
          <button>
            发布
          </button>
        </div>
      </div>
    )
  }
}