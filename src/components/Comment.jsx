import React, { Component } from 'react'
import PropTypes from 'prop-types'

class Comment extends Component {
  static propTypes = {
    comment: PropTypes.object.isRequired,
    onDeleteComment: PropTypes.func,
    index: PropTypes.number
  }

  constructor() {
    super()
    this.state = { timeString: '' }
  }

  /**
   * 加载组件就拉起解析时间，
   * 开启定时器5秒刷新一次
   */
  componentWillMount() {
    this._updateTimeString()
    this._timer = setInterval(
      this._updateTimeString.bind(this),
      5000
    )
  }

  /**
   * 组件销毁时清空定时器
   */
  componentWillUnmount() {
    clearInterval(this._timer)
  }

  /**
   * 时间解析，小于一分钟算秒，大于几时分钟
   */
  _updateTimeString() {
    const comment = this.props.comment
    const duration = (+Date.now() - comment.createdTime) / 1000
    this.setState({
      timeString: duration > 60
        ? `${Math.round(duration / 60)} 分钟前`
        : `${Math.round(Math.max(duration, 1))} 秒前`
    })
  }

  /**
   * html显示代码块
   * @param {*} content 
   */
  _getProcessedContent(content) {
    return content
      .replace(/&/g, "&amp;")
      .replace(/</g, "&lt;")
      .replace(/>/g, "&gt;")
      .replace(/"/g, "&quot;")
      .replace(/'/g, "&#039;")
      .replace(/`([\S\s]+?)`/g, '<code>$1</code>')
  }

  /**
   * 删除组件函数
   */
  handleDeleteComment() {
    // 判断父级函数调用
    if (this.props.onDeleteComment) {
      this.props.onDeleteComment(this.props.index)
    }
  }

  render() {
    const comment = this.props.comment;
    return (
      <div className='comment'>
        <div className='comment-user'>
          <span className='comment-username'>{ comment.username } </span>：
        </div>
        <p dangerouslySetInnerHTML={ {
          __html: this._getProcessedContent(comment.content)
        } } />
        <span className='comment-createdtime'>
          { this.state.timeString }
        </span>
        <span className='comment-delete' onClick={ this.handleDeleteComment.bind(this) }>
          删除
        </span>
      </div>
    )
  }
}

export default Comment
