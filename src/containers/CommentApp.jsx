import React, { Component } from 'react'
import CommentInput from './CommentInput.jsx'
import CommentList from './CommentList.jsx'

export default class CommentApp extends Component {
  render() {
    return (
      <div className='wrapper'>
        <CommentInput />
        <CommentList />
      </div>
    )
  }
}