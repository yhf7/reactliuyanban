# React 小书 评论功能案例

# 架构
- create-react-app

# 功能
### 第一阶段
- [x] 用户，评论输入框
- [x] 实现发布
- [x] 实现内容列表

### 第二阶段
- [x] 页面加载完成自动聚焦到评论输入框。
- [x] 把用户名持久化，存放到浏览器的 LocalStorage 中。页面加载时会把用户名加载出来显示到输入框，用户就不需要重新输入用户名了。
- [x] 把已经发布的评论持久化，存放到浏览器的 LocalStorage 中。页面加载时会把已经保存的评论加载出来，显示到页面的评论列表上。
- [x] 评论显示发布日期，如“1 秒前”，”30 分钟前”，并且会每隔 5 秒更新发布日期。
- [x] 评论可以被删除。
- [x] 类似 Markdown 的行内代码块显示功能，用户输入的用 `` 包含起来的内容都会被处理成用 <code> 元素包含。例如输入 `console.log` 就会处理成 <code>console.log</code> 再显示到页面上。

### 第三阶段
- [x] 分开 reducers \ Dumb \ Smart
- 使用 Redux 和 react-redux 重构

# 项目使用
1. 下载
``` bash
$ git clone git@gitee.com:yhf7/reactliuyanban.git
```

2. 装包
``` bash
$ sudo cnpm i (yarn npm 等都可以，mac前面加sudo，win不加)
```

3. 启动
``` bash
$ yarn start / npm run start
```

